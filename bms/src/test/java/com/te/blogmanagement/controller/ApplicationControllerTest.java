package com.te.blogmanagement.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.UnsupportedEncodingException;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.neo4j.Neo4jProperties.Authentication;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.te.blogmanagement.dto.LoginDto;
import com.te.blogmanagement.dto.UserDto;
import com.te.blogmanagement.response.ApiResponse;
import com.te.blogmanagement.service.BmService;

import lombok.var;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class ApplicationControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private BmService bmService;

	@InjectMocks
	private ApplicationController bmController;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private WebApplicationContext webApplicationContext;

	private PasswordEncoder passwordEncoder;

	private AuthenticationManager authenticationManager;

	@Test
	@WithMockUser
	void testLoginUser() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		LoginDto loginDto = LoginDto.builder().username("VNY").password("12345").build();

		/*
		 * Mockito.when(bmService.login(Mockito.any())).thenReturn(true); String
		 * contentAsString = mockMvc
		 * .perform(MockMvcRequestBuilders.get("/public/login")
		 * .accept(MediaType.APPLICATION_JSON_VALUE)
		 * .contentType(MediaType.APPLICATION_JSON_VALUE)
		 * .content(objectMapper.writeValueAsString(loginDto)))
		 * .andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().
		 * getContentAsString(); ApiResponse<String> readValue =
		 * objectMapper.readValue(contentAsString, ApiResponse.class);
		 * assertEquals("Login successfull", readValue.getMessage());
		 */
		Authentication authentication = new Authentication();
		Mockito.when(authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword())))
				.thenReturn((org.springframework.security.core.Authentication) authentication);
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.get("/public/login").accept(MediaType.APPLICATION_JSON_VALUE)
						.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpectAll(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		ApiResponse<String> readValue = objectMapper.readValue(contentAsString, ApiResponse.class);
		assertEquals("Login successfull", readValue.getMessage());

	}

	@Test
	@WithMockUser
	void testLoginUser_Returns400() throws Exception {
		LoginDto loginDto = LoginDto.builder().username("VNY").password("12345").build();
		Mockito.when(bmService.login(Mockito.any())).thenReturn(false);
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.get("/public/login").accept(MediaType.APPLICATION_JSON_VALUE)
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(loginDto)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();

	}

	@Test
	@WithMockUser
	public void testuserRegistration() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		UserDto userDto = UserDto.builder().email("tej123@gmail.com").firstName("ravi").lastName("teja")
				.middleName("vasa").intro("this is tej").mobile("9876543210").passwordHash("qwerty").postDto(null)
				.profile("asdfghj").build();
		Mockito.when(bmService.registration(Mockito.any())).thenReturn(Optional.ofNullable(userDto));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.post("/public/userRegistration")
						.accept(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(userDto))
						.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		ApiResponse readValue = objectMapper.readValue(contentAsString, ApiResponse.class);
		assertEquals("Registration Successfull", readValue.getMessage());
	}

	@Test
	@WithMockUser
	public void testuserRegistration_Returns400() throws JsonProcessingException, Exception {
		UserDto userDto = UserDto.builder().email("tej123@gmail.com").firstName("ravi").lastName("teja")
				.middleName("vasa").intro("this is tej").mobile("9876543210").passwordHash("qwerty").postDto(null)
				.profile("asdfghj").build();
		Mockito.when(bmService.registration(Mockito.any())).thenReturn(Optional.ofNullable(null));

		mockMvc.perform(MockMvcRequestBuilders.post("/public/userRegistration").accept(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(userDto)).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

}
