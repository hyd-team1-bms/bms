package com.te.blogmanagement.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.common.collect.Lists;
import com.te.blogmanagement.dto.UserDto;
import com.te.blogmanagement.response.ApiResponse;
import com.te.blogmanagement.service.BmService;

@SpringBootTest
@AutoConfigureMockMvc
class AdminControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private BmService bmService;

	@InjectMocks
	private AdminController adminController;

	private ObjectMapper objectMapper = new ObjectMapper();

	@Test
	@WithMockUser()
	public void testUserDataList() throws UnsupportedEncodingException, Exception {

		objectMapper.registerModule(new JavaTimeModule());
		List<UserDto> ud = Lists.newArrayList();
		UserDto userDto = UserDto.builder().email("tej123@gmail.com").firstName("ravi").lastName("teja")
				.middleName("vasa").intro("this is tej").mobile("9876543210").passwordHash("qwerty").postDto(null)
				.profile("asdfghj").build();
		ud.add(userDto);
		Mockito.when(bmService.UserDataList()).thenReturn(Optional.ofNullable(ud));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.get("/auth/userDataList").contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(userDto)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		ApiResponse<Object> readValue = objectMapper.readValue(contentAsString, ApiResponse.class);
		assertEquals("User DataList is Available", readValue.getMessage());
	}

	@Test
	@WithMockUser
	public void testUserDataList_Returns400() throws UnsupportedEncodingException, Exception {
		Mockito.when(bmService.UserDataList()).thenReturn(Optional.ofNullable(null));
		mockMvc.perform(MockMvcRequestBuilders.get("/auth/userDataList").accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(MockMvcResultMatchers.status().isBadRequest())
				.andReturn().getResponse().getContentAsString();
	}

	@Test
	@WithMockUser
	void testDeleteUser() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		Mockito.when(bmService.deleteUser(Mockito.any())).thenReturn(Optional.ofNullable(true));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/auth/deleteUser/1").accept(MediaType.APPLICATION_JSON_VALUE)
						.contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(true)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		ApiResponse<String> readValue = objectMapper.readValue(contentAsString, ApiResponse.class);
		assertEquals("Delete User Successfull", readValue.getMessage());

	}

	@Test
	@WithMockUser
	void testDeleteUser_Return400() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		Mockito.when(bmService.deleteUser(Mockito.any())).thenReturn(Optional.ofNullable(false));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/auth/deleteUser/1").accept(MediaType.APPLICATION_JSON_VALUE)
						.contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(true)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();

	}
}
