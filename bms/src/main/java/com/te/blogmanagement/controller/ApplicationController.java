package com.te.blogmanagement.controller;

import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.te.blogmanagement.customexception.UserRegistrationUnsuccessfullExcption;
import com.te.blogmanagement.dto.LoginDto;
import com.te.blogmanagement.dto.UserDto;
import com.te.blogmanagement.response.ApiResponse;
import com.te.blogmanagement.service.BmService;
import com.te.blogmanagement.utils.jwt.JwtUtils;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/public")
public class ApplicationController {
	private final BmService bmService;
	private final AuthenticationManager authenticationManager;
	private final JwtUtils jwtUtils;

	@GetMapping(path = "/login")
	public ResponseEntity<ApiResponse<Object>> login(@RequestBody LoginDto loginDto) {
		authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
		String token = jwtUtils.generateToken(loginDto.getUsername());
		return ResponseEntity.ok().body(ApiResponse.builder().message("Login successfull").token(token).build());

	}

	@PostMapping(path = "/userRegistration")
	public ResponseEntity<ApiResponse<Object>> userRegistration(@RequestBody UserDto userDto) {
		Optional<UserDto> registration = bmService.registration(userDto);
		if (registration.isPresent()) {
			return ResponseEntity.ok().body(ApiResponse.builder().message("Registration Successfull").build());
		}
		throw new UserRegistrationUnsuccessfullExcption("Registration Unsuccessfull");
	}

}
