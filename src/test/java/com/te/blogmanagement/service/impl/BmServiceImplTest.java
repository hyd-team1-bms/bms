package com.te.blogmanagement.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.collect.Lists;
import com.te.blogmanagement.dto.LoginDto;
import com.te.blogmanagement.dto.PostCommentDto;
import com.te.blogmanagement.dto.PostDto;
import com.te.blogmanagement.dto.UpdatePostCommentDto;
import com.te.blogmanagement.dto.UpdatePostDto;
import com.te.blogmanagement.dto.UserDto;
import com.te.blogmanagement.dto.UserUpdateDto;
import com.te.blogmanagement.entity.Admin;
import com.te.blogmanagement.entity.AppUser;
import com.te.blogmanagement.entity.Post;
import com.te.blogmanagement.entity.PostComment;
import com.te.blogmanagement.entity.Roles;
import com.te.blogmanagement.entity.User;
import com.te.blogmanagement.enums.Status;
import com.te.blogmanagement.repository.AdminRepository;
import com.te.blogmanagement.repository.AppUserRepository;
import com.te.blogmanagement.repository.PostCommentRepository;
import com.te.blogmanagement.repository.PostRepository;
import com.te.blogmanagement.repository.RoleRepository;
import com.te.blogmanagement.repository.TagRepository;
import com.te.blogmanagement.repository.UserRepository;
import com.te.blogmanagement.service.BmService;

@SpringBootTest
class BmServiceImplTest {

	@Mock
	private UserRepository userRepository;
	@Mock
	private PostRepository postRepository;
	@Mock
	private AppUserRepository appUserRepository;
	@Mock
	private PostCommentRepository postCommentRepository;
	@Mock
	private TagRepository tagRepository;
	@Mock
	private JavaMailSender javaMailSender;
	@Mock
	private RoleRepository roleRepository;
	@Mock
	private PasswordEncoder passwordEncoder;
	@Mock
	private AdminRepository adminRepository;

	@InjectMocks
	private BmServiceImpl bmServiceImpl;

	@Autowired
	private BmService bmService;

	@BeforeEach
	public void setUp() {
		Roles user = Roles.builder().roleName("ROLE_USER").build();
		Roles admin = Roles.builder().roleName("ROLE_ADMIN").appUser(Lists.newArrayList()).build();

		Admin adminCredentials = Admin.builder().adminId("adminId").adminName("adminName").build();

		AppUser appUser = AppUser.builder().userName(adminCredentials.getAdminId())
				.password(passwordEncoder.encode("qwerty")).roles(Lists.newArrayList()).build();
		roleRepository.save(user);
		roleRepository.save(admin);

		adminRepository.save(adminCredentials);

		admin.getAppUser().add(appUser);
		appUser.getRoles().add(admin);
		roleRepository.save(admin);
		appUserRepository.save(appUser);
	}

	@Test
	public void testRegistration() {
		UserDto userDto = UserDto.builder().email("tej123@gmail.com").firstName("ravi").middleName("tej").lastName("v")
				.intro(null).mobile("987654321").passwordHash("qwerty").postDto(null).profile("this is RTV").build();
		User user = User.builder().email("tej123@gmail.com").firstName("ravi").middleName("tej").lastName("v")
				.intro(null).mobile("987654321").passwordHash("qwerty").post(null).profile("this is RTV").build();
		AppUser appUser = AppUser.builder().password("qwerty").roles(Lists.newArrayList()).build();
		Roles roles = Roles.builder().roleName("ROLE_USER").appUser(Lists.newArrayList()).build();
		appUser.getRoles().add(roles);
		roles.getAppUser().add(appUser);
		Mockito.when(roleRepository.findByRoleName(Mockito.any())).thenReturn(Optional.ofNullable(roles));
		Mockito.when(appUserRepository.save(Mockito.any())).thenReturn(appUser);
		Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(user));

		Optional<User> findById = userRepository.findById(1);
		Optional<UserDto> registration = bmServiceImpl.registration(userDto);
		bmServiceImpl.sendEmail("", "", "");
		assertEquals(registration.get().getFirstName(), user.getFirstName());
	}

	@Test
	public void testRegistration_ReturnsFalse() {
		UserDto userDto = UserDto.builder().email("tej123@gmail.com").firstName("ravi").middleName("tej").lastName("v")
				.intro(null).mobile("987654321").passwordHash("qwerty").postDto(null).profile("this is RTV").build();
		User user = User.builder().email("tej123@gmail.com").firstName("ravi").middleName("tej").lastName("v")
				.intro(null).mobile("987654321").passwordHash("qwerty").post(null).profile("this is RTV").build();
		AppUser appUser = AppUser.builder().password("qwerty").roles(Lists.newArrayList()).build();
		Roles roles = Roles.builder().roleName("ROLE_USER").appUser(Lists.newArrayList()).build();
		appUser.getRoles().add(roles);
		roles.getAppUser().add(appUser);
		Mockito.when(roleRepository.findByRoleName(Mockito.any())).thenReturn(Optional.ofNullable(null));
		Mockito.when(appUserRepository.save(Mockito.any())).thenReturn(appUser);
		Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(user));

		Optional<User> findById = userRepository.findById(1);
		Optional<UserDto> registration = bmServiceImpl.registration(userDto);
		bmServiceImpl.sendEmail("", "", "");
		assertTrue(registration.isEmpty());
	}

	@Test
	public void testUpdateUser_ReturnsTrue() {
		User user = User.builder().email("tej123@gmail.com").firstName("ravi").middleName("tej").lastName("v")
				.intro(null).mobile("987654321").passwordHash("qwerty").post(null).profile("this is RTV").build();
		AppUser appUser = AppUser.builder().password("qwerty").roles(Lists.newArrayList()).build();
		UserUpdateDto userUpdateDto = UserUpdateDto.builder().email("tej123@gmail.com").firstName("ravi")
				.middleName("tej").lastName("v").intro(null).mobile("987654321").passwordHash("qwerty")
				.profile("this is RTV").build();
		Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(user));
		Mockito.when(userRepository.save(user)).thenReturn(user);
		assertEquals(bmServiceImpl.updateUser(Mockito.any(), userUpdateDto), Optional.ofNullable(true));
	}

	@Test
	public void testUpdateUser_ReturnsFalse() {
		User user = User.builder().email("tej123@gmail.com").firstName("ravi").middleName("tej").lastName("v")
				.intro(null).mobile("987654321").passwordHash("qwerty").post(null).profile("this is RTV").build();
		AppUser appUser = AppUser.builder().password("qwerty").roles(Lists.newArrayList()).build();
		UserUpdateDto userUpdateDto = UserUpdateDto.builder().email("tej123@gmail.com").firstName("ravi")
				.middleName("tej").lastName("v").intro(null).mobile("987654321").passwordHash("qwerty")
				.profile("this is RTV").build();
		Mockito.when(userRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(null));
		Mockito.when(appUserRepository.save(appUser)).thenReturn(appUser);
		assertFalse(bmServiceImpl.updateUser(Mockito.anyInt(), userUpdateDto).get());
	}

	@Test
	public void testLogin_ReturnsTrue() {
		LoginDto loginDto = LoginDto.builder().username("987654321").password("qwerty").build();
		AppUser appUser = AppUser.builder().password("qwerty").roles(Lists.newArrayList()).build();
		User user = User.builder().email("tej123@gmail.com").firstName("ravi").middleName("tej").lastName("v")
				.intro(null).mobile("987654321").passwordHash("qwerty").post(null).profile("this is RTV")
				.status(Status.ACTIVE).build();
		Mockito.when(appUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(appUser));
		Mockito.when(userRepository.findByMobile(Mockito.anyString())).thenReturn(user);
		Mockito.when(userRepository.save(user)).thenReturn(user);
		assertTrue(bmServiceImpl.login(loginDto));
	}

	@Test
	public void testLogin_ReturnsFalse() {
		LoginDto loginDto = LoginDto.builder().username("987654321").password("qwerty").build();
		AppUser appUser = AppUser.builder().password("qwerty").roles(Lists.newArrayList()).build();
		User user = User.builder().email("tej123@gmail.com").firstName("ravi").middleName("tej").lastName("v")
				.intro(null).mobile("987654321").passwordHash("qwerty").post(null).profile("this is RTV")
				.status(Status.ACTIVE).build();
		Mockito.when(appUserRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(null));
		Mockito.when(userRepository.findByMobile(Mockito.anyString())).thenReturn(user);
		Mockito.when(userRepository.save(user)).thenReturn(user);
		assertFalse(bmServiceImpl.login(loginDto));
	}

	@Test
	public void testUserData() {
		User user = User.builder().email("a@a").firstName("vinay").id(11).intro("intro").lastName("thoutam")
				.middleName("kumar").passwordHash("password").post(Lists.newArrayList()).profile(null).build();

		Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(user));

		assertEquals(user.getFirstName(), bmServiceImpl.userData(1).get().getFirstName());

	}

	@Test
	public void testUserData_Returns400() {
		User user = User.builder().email("a@a").firstName("vinay").id(11).intro("intro").lastName("thoutam")
				.middleName("kumar").passwordHash("password").post(Lists.newArrayList()).profile(null).build();

		Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(null));
		Optional<UserDto> data = bmServiceImpl.userData(1);
		boolean empty = data.isEmpty();
		assertEquals(true, bmServiceImpl.userData(Mockito.anyInt()).isEmpty());
	}

	@Test
	public void testDeleteUser() {
		User user = User.builder().email("a@a").firstName("vinay").id(11).intro("intro").lastName("thoutam")
				.middleName("kumar").passwordHash("password").post(Lists.newArrayList()).profile(null).build();

		Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(user));
		Mockito.when(userRepository.save(user)).thenReturn(user);

		assertTrue(bmServiceImpl.deleteUser(Mockito.anyInt()).get());
	}

	@Test
	public void testDeleteUser_returnFalse() {
		User user = User.builder().email("a@a").firstName("vinay").id(11).intro("intro").lastName("thoutam")
				.middleName("kumar").passwordHash("password").post(Lists.newArrayList()).profile(null).build();

		Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(null));
//		Mockito.when(userRepository.save(user)).thenReturn(user);

		assertFalse(bmServiceImpl.deleteUser(Mockito.anyInt()).get());
	}

	@Test
	public void testUpdatePost_ReturnTrue() {
		Post post = Post.builder().id(1).authorId(1).parentId(1).title("efa").metaTitle("wed").slug("qwas")
				.summary("wqd").published("wdqw").createdAt(LocalDateTime.now()).updatedAt(LocalDateTime.now())
				.publishedAt(LocalDateTime.now()).content("qwaswasx").postStatus(Status.ACTIVE).data(null)
				.fileType("waswds").fileName("dwaewfdc").build();
		UpdatePostDto updatePostDto = UpdatePostDto.builder().authorId(1).parentId(1).title("efa").metaTitle("wed")
				.slug("qwas").summary("wqd").updatedAt(LocalDateTime.now()).content("qwaswasx").build();
		Mockito.when(postRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(post));
		Mockito.when(postRepository.save(Mockito.any())).thenReturn(post);
		// Optional<Boolean> updatePost = bmService.updatePost(Mockito.anyInt(),
		// updatePostDto);
		assertTrue(bmServiceImpl.updatePost(Mockito.anyInt(), updatePostDto).get());
	}

	@Test
	public void testUpdatePost_ReturnFalse() {
		Post post = Post.builder().id(1).authorId(1).parentId(1).title("efa").metaTitle("wed").slug("qwas")
				.summary("wqd").published("wdqw").createdAt(LocalDateTime.now()).updatedAt(LocalDateTime.now())
				.publishedAt(LocalDateTime.now()).content("qwaswasx").postStatus(Status.INACTIVE).data(null)
				.fileType("waswds").fileName("dwaewfdc").build();
		UpdatePostDto updatePostDto = UpdatePostDto.builder().authorId(1).parentId(1).title("efa").metaTitle("wed")
				.slug("qwas").summary("wqd").updatedAt(LocalDateTime.now()).content("qwaswasx").build();
		Mockito.when(postRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(null));
		assertFalse(bmServiceImpl.updatePost(Mockito.anyInt(), updatePostDto).get());
	}

	@Test
	public void testDeletePost_ReturnTrue() {
		Post post = Post.builder().id(1).authorId(1).parentId(1).title("efa").metaTitle("wed").slug("qwas")
				.summary("wqd").published("wdqw").createdAt(LocalDateTime.now()).updatedAt(LocalDateTime.now())
				.publishedAt(LocalDateTime.now()).content("qwaswasx").postStatus(Status.ACTIVE).data(null)
				.fileType("waswds").fileName("dwaewfdc").build();
		Mockito.when(postRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(post));
		Mockito.when(postRepository.save(post)).thenReturn(post);
		assertTrue(bmServiceImpl.deletePost(Mockito.anyInt()).get());
	}

	@Test
	public void testDeletePost_ReturnFalse() {
		Post post = Post.builder().id(1).authorId(1).parentId(1).title("efa").metaTitle("wed").slug("qwas")
				.summary("wqd").published("wdqw").createdAt(LocalDateTime.now()).updatedAt(LocalDateTime.now())
				.publishedAt(LocalDateTime.now()).content("qwaswasx").postStatus(Status.INACTIVE).data(null)
				.fileType("waswds").fileName("dwaewfdc").build();
		Mockito.when(postRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(null));
		assertFalse(bmServiceImpl.deletePost(Mockito.anyInt()).get());

	}

	@Test
	public void testCommentRegistration_returnTrue() {
		Post post = Post.builder().id(1).authorId(1).parentId(1).title("efa").metaTitle("wed").slug("qwas")
				.summary("wqd").published("wdqw").createdAt(LocalDateTime.now()).updatedAt(LocalDateTime.now())
				.publishedAt(LocalDateTime.now()).content("qwaswasx").postStatus(Status.ACTIVE).data(null)
				.fileType("waswds").fileName("dwaewfdc").build();
		PostComment postComment = PostComment.builder().id(1).postId("te-01").parentId("te-02").title("asfAF")
				.published("WEGw").createdAt(LocalDateTime.now()).publishedAt(LocalDateTime.now()).content("bihb")
				.status(Status.ACTIVE).build();
		PostCommentDto postCommentDto = PostCommentDto.builder().postId("te-01").parentId("te-02").title("asfAF")
				.published("WEGw").createdAt(LocalDateTime.now()).publishedAt(LocalDateTime.now()).content("bihb")
				.build();
		Mockito.when(postRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(post));
		Mockito.when(postCommentRepository.save(postComment)).thenReturn(postComment);
		assertTrue(bmServiceImpl.commentRegistration(Mockito.anyInt(), postCommentDto).get());
	}

	@Test
	public void testCommentRegistration_returnFalse() {
		Post post = Post.builder().id(1).authorId(1).parentId(1).title("efa").metaTitle("wed").slug("qwas")
				.summary("wqd").published("wdqw").createdAt(LocalDateTime.now()).updatedAt(LocalDateTime.now())
				.publishedAt(LocalDateTime.now()).content("qwaswasx").postStatus(Status.ACTIVE).data(null)
				.fileType("waswds").fileName("dwaewfdc").build();
		PostCommentDto postCommentDto = PostCommentDto.builder().postId("te-01").parentId("te-02").title("asfAF")
				.published("WEGw").createdAt(LocalDateTime.now()).publishedAt(LocalDateTime.now()).content("bihb")
				.build();
		Mockito.when(postRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(null));
		assertFalse(bmServiceImpl.commentRegistration(Mockito.anyInt(), postCommentDto).get());
	}

	@Test
	public void testUpdatePostComment_returnTrue() {
		PostComment postComment = PostComment.builder().id(1).postId("te-01").parentId("te-02").title("asfAF")
				.published("WEGw").createdAt(LocalDateTime.now()).publishedAt(LocalDateTime.now()).content("bihb")
				.status(Status.ACTIVE).build();
		UpdatePostCommentDto updatePostCommentDto = UpdatePostCommentDto.builder().parentId("te-02").title("asfAF")
				.published("WEGw").publishedAt(LocalDateTime.now()).content("bihb").build();

		Mockito.when(postCommentRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(postComment));
		Mockito.when(postCommentRepository.save(postComment)).thenReturn(postComment);
		assertTrue(bmServiceImpl.updatePostComment(Mockito.anyInt(), updatePostCommentDto).get());

	}

	@Test
	public void testUpdatePostComment_returnFalse() {
		PostComment postComment = PostComment.builder().id(1).postId("te-01").parentId("te-02").title("asfAF")
				.published("WEGw").createdAt(LocalDateTime.now()).publishedAt(LocalDateTime.now()).content("bihb")
				.status(Status.ACTIVE).build();
		UpdatePostCommentDto updatePostCommentDto = UpdatePostCommentDto.builder().parentId("te-02").title("asfAF")
				.published("WEGw").publishedAt(LocalDateTime.now()).content("bihb").build();

		Mockito.when(postCommentRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(null));
		assertFalse(bmServiceImpl.updatePostComment(Mockito.anyInt(), updatePostCommentDto).get());
	}

	@Test
	public void testDeletePostCommen_returnTrue() {
		PostComment postComment = PostComment.builder().id(1).postId("te-01").parentId("te-02").title("asfAF")
				.published("WEGw").createdAt(LocalDateTime.now()).publishedAt(LocalDateTime.now()).content("bihb")
				.status(Status.ACTIVE).build();
		Mockito.when(postCommentRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(postComment));
		Mockito.when(postCommentRepository.save(postComment)).thenReturn(postComment);
		assertTrue(bmServiceImpl.deletePostComment(Mockito.anyInt()).get());
	}

	@Test
	public void testDeletePostComment_returnFalse() {
		Mockito.when(postCommentRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(null));
		assertFalse(bmServiceImpl.deletePostComment(Mockito.anyInt()).get());
	}

	@Test
	public void testUserDataList_returnTrue() {
		List<User> user = new ArrayList<User>();
		user.add(new User(1, "gvg", "vjgfu", "fdty", "487451", "cdt@gmail.com", "jhvvhb", LocalDateTime.now(),
				LocalDateTime.now(), "gitif", "ygyi", Status.ACTIVE, null));
		List<UserDto> userDto = new ArrayList<UserDto>();
		userDto.add(new UserDto("gvg", "vjgfu", "fdty", "487451", "cdt@gmail.com", "jhvvhb", "gitif", "ygyi", null));
		Mockito.when(userRepository.findAll()).thenReturn(user);
		assertEquals(userDto.get(0).getFirstName(), bmServiceImpl.UserDataList().get().get(0).getFirstName());
	}

	@Test
	public void testUserDataList_returnFalse() {
		Mockito.when(userRepository.findAll()).thenReturn(null);
		assertEquals(true, bmServiceImpl.UserDataList().isEmpty());
	}

	@Test
	public void testGetFile() {
		Post post = Post.builder().id(1).authorId(1).parentId(1).title("efa").metaTitle("wed").slug("qwas")
				.summary("wqd").published("wdqw").createdAt(LocalDateTime.now()).updatedAt(LocalDateTime.now())
				.publishedAt(LocalDateTime.now()).content("qwaswasx").postStatus(Status.ACTIVE).data(null)
				.fileType("waswds").fileName("dwaewfdc").build();
		Mockito.when(postRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(post));
		PostDto postDto = new PostDto();
		BeanUtils.copyProperties(post, postDto);
		assertEquals(1, bmServiceImpl.getfile(1).get().getAuthorId());
	}

	@Test
	public void testUploadFile() {
		Post post = Post.builder().id(1).authorId(1).parentId(1).title("efa").metaTitle("wed").slug("qwas")
				.summary("wqd").published("wdqw").createdAt(LocalDateTime.now()).updatedAt(LocalDateTime.now())
				.publishedAt(LocalDateTime.now()).content("qwaswasx").postStatus(Status.ACTIVE).data(null)
				.fileType("waswds").fileName("dwaewfdc").build();
		MultipartFile multipartFile = new MultipartFile() {

			@Override
			public byte[] getBytes() throws IOException {
				return new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
			}

			@Override
			public String getName() {
				return null;
			}

			@Override
			public String getOriginalFilename() {
				return null;
			}

			@Override
			public String getContentType() {
				return null;
			}

			@Override
			public boolean isEmpty() {
				return false;
			}

			@Override
			public long getSize() {
				return 0;
			}

			@Override
			public InputStream getInputStream() throws IOException {
				return null;
			}

			@Override
			public void transferTo(File dest) throws IOException, IllegalStateException {
			}
		};
		Mockito.when(postRepository.save(Mockito.any())).thenReturn(post);
		String fileDownloaduri = ServletUriComponentsBuilder.fromCurrentContextPath().path("public/downloadFile/")
				.path(post.getId() + "").toUriString();
		assertEquals("http://localhost/public/downloadFile/1", bmServiceImpl.uploadFile(multipartFile).get());
	}

	@Test
	public void testReadPostAndComments() {
		User user = User.builder().email("tej123@gmail.com").firstName("ravi").middleName("tej").lastName("v")
				.intro(null).mobile("987654321").passwordHash("qwerty").post(null).profile("this is RTV").build();
		Post post = Post.builder().id(1).authorId(1).parentId(1).title("efa").metaTitle("wed").slug("qwas")
				.postComment(Lists.newArrayList()).summary("wqd").published("wdqw").createdAt(LocalDateTime.now())
				.updatedAt(LocalDateTime.now()).publishedAt(LocalDateTime.now()).content("qwaswasx")
				.postStatus(Status.ACTIVE).data(null).fileType("waswds").fileName("dwaewfdc").build();
		Mockito.when(userRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(user));
		Mockito.when(postRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(post));
		assertEquals(1, bmServiceImpl.readPostAndComments(1).get().getAuthorId());
	}

	@Test
	public void testGetFileReturnFalse() {
		Post post = Post.builder().id(1).authorId(1).parentId(1).title("efa").metaTitle("wed").slug("qwas")
				.summary("wqd").published("wdqw").createdAt(LocalDateTime.now()).updatedAt(LocalDateTime.now())
				.publishedAt(LocalDateTime.now()).content("qwaswasx").postStatus(Status.ACTIVE).data(null)
				.fileType("waswds").fileName("dwaewfdc").build();
		Mockito.when(postRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(post));
		PostDto postDto = new PostDto();
		BeanUtils.copyProperties(post, postDto);
		assertNotEquals(12123, bmServiceImpl.getfile(1).get().getAuthorId());
	}

	@Test
	public void testUploadFilReturnFalse() {
		Post post = Post.builder().id(1).authorId(1).parentId(1).title("efa").metaTitle("wed").slug("qwas")
				.summary("wqd").published("wdqw").createdAt(LocalDateTime.now()).updatedAt(LocalDateTime.now())
				.publishedAt(LocalDateTime.now()).content("qwaswasx").postStatus(Status.ACTIVE).data(null)
				.fileType("waswds").fileName("dwaewfdc").build();
		MultipartFile multipartFile = new MultipartFile() {

			@Override
			public byte[] getBytes() throws IOException {
				return new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
			}

			@Override
			public String getName() {
				return null;
			}

			@Override
			public String getOriginalFilename() {
				return null;
			}

			@Override
			public String getContentType() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isEmpty() {
				return false;
			}

			@Override
			public long getSize() {
				return 0;
			}

			@Override
			public InputStream getInputStream() throws IOException {
				return null;
			}

			@Override
			public void transferTo(File dest) throws IOException, IllegalStateException {
			}
		};
		Mockito.when(postRepository.save(Mockito.any())).thenReturn(post);
		String fileDownloaduri = ServletUriComponentsBuilder.fromCurrentContextPath().path("public/downloadFile/")
				.path(post.getId() + "").toUriString();
		assertNotEquals("http://localhost/public/downloadFile/123", bmServiceImpl.uploadFile(multipartFile).get());
	}

	@Test
	public void testReadPostAndCommentsReturnFalse() {
		User user = User.builder().email("tej123@gmail.com").firstName("ravi").middleName("tej").lastName("v")
				.intro(null).mobile("987654321").passwordHash("qwerty").post(null).profile("this is RTV").build();
		Post post = Post.builder().id(1).authorId(1).parentId(1).title("efa").metaTitle("wed").slug("qwas")
				.postComment(Lists.newArrayList()).summary("wqd").published("wdqw").createdAt(LocalDateTime.now())
				.updatedAt(LocalDateTime.now()).publishedAt(LocalDateTime.now()).content("qwaswasx")
				.postStatus(Status.ACTIVE).data(null).fileType("waswds").fileName("dwaewfdc").build();
		Mockito.when(userRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(user));
		Mockito.when(postRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(post));
		assertNotEquals(21, bmServiceImpl.readPostAndComments(1).get().getAuthorId());
	}

}
