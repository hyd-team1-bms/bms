package com.te.blogmanagement.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.te.blogmanagement.dto.LoginDto;
import com.te.blogmanagement.dto.PostCommentDto;
import com.te.blogmanagement.dto.PostDto;
import com.te.blogmanagement.dto.UpdatePostCommentDto;
import com.te.blogmanagement.dto.UpdatePostDto;
import com.te.blogmanagement.dto.UserDto;
import com.te.blogmanagement.dto.UserUpdateDto;
import com.te.blogmanagement.response.ApiResponse;
import com.te.blogmanagement.service.BmService;

@SpringBootTest
@AutoConfigureMockMvc
class BmControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private BmService bmService;

	@InjectMocks
	private BmController bmController;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private WebApplicationContext webApplicationContext;


	@Test
	@WithMockUser
	public void testRegisterBatch_Returns400() throws JsonProcessingException, Exception {
		UserDto userDto = UserDto.builder().email("tej123@gmail.com").firstName("ravi").lastName("teja")
				.middleName("vasa").intro("this is tej").mobile("9876543210").passwordHash("qwerty").postDto(null)
				.profile("asdfghj").build();
		Mockito.when(bmService.registration(Mockito.any())).thenReturn(Optional.ofNullable(null));

		mockMvc.perform(MockMvcRequestBuilders.post("/public/userRegistration").accept(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(userDto)).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	@Test
	@WithMockUser
	public void testUserData_Returns400() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		UserDto userDto = UserDto.builder().email("tej123@gmail.com").firstName("ravi").lastName("teja")
				.middleName("vasa").intro("this is tej").mobile("9876543210").passwordHash("qwerty").postDto(null)
				.profile("asdfghj").build();
		Mockito.when(bmService.registration(Mockito.any())).thenReturn(Optional.ofNullable(null));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.get("/auth/user/userData/1").accept(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(userDto))
						.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();

	}

	@Test
	@WithMockUser
	public void testUserData() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		UserDto userDto = UserDto.builder().email("tej123@gmail.com").firstName("ravi").lastName("teja")
				.middleName("vasa").intro("this is tej").mobile("9876543210").passwordHash("qwerty").postDto(null)
				.profile("asdfghj").build();
		Mockito.when(bmService.userData(Mockito.any())).thenReturn(Optional.ofNullable(userDto));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.get("/auth/user/userData/1").accept(MediaType.APPLICATION_JSON_VALUE)
						.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		ApiResponse readValue = objectMapper.readValue(contentAsString, ApiResponse.class);
		assertEquals("User Data Is Available", readValue.getMessage());

	}

	

	@Test
	@WithMockUser
	public void testPostUpdate() throws UnsupportedEncodingException, Exception {
		UpdatePostDto updatePostDto = UpdatePostDto.builder().authorId(1).content("learn programming").metaTitle("fghj")
				.slug("gh/gh").title(null).build();
		Mockito.when(bmService.updatePost(Mockito.any(), Mockito.any())).thenReturn(Optional.ofNullable(true));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/auth/user/postUpdate/1").accept(MediaType.APPLICATION_JSON_VALUE)
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(updatePostDto)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		ApiResponse readValue = objectMapper.readValue(contentAsString, ApiResponse.class);
		assertEquals("update post successfull", readValue.getMessage());
	}

	@Test
	@WithMockUser
	public void testPostUpdate_Returns400() throws UnsupportedEncodingException, Exception {
		UpdatePostDto updatePostDto = UpdatePostDto.builder().authorId(1).content("learn programming").metaTitle("fghj")
				.slug("gh/gh").title(null).build();
		Mockito.when(bmService.updatePost(Mockito.any(), Mockito.any())).thenReturn(Optional.ofNullable(false));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/auth/user/postUpdate/1").accept(MediaType.APPLICATION_JSON_VALUE)
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(updatePostDto)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();
	}

	@Test
	@WithMockUser
	public void deletePost() throws UnsupportedEncodingException, Exception {
		Mockito.when(bmService.deletePost(Mockito.any())).thenReturn(Optional.ofNullable(true));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.delete("/auth/user/deletePost/1").accept(MediaType.APPLICATION_JSON_VALUE)
						.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		ApiResponse readValue = objectMapper.readValue(contentAsString, ApiResponse.class);
		assertEquals("Delete Post Successfull", readValue.getMessage());
	}

	@Test
	@WithMockUser
	public void deletePost_Returns400() throws UnsupportedEncodingException, Exception {
		Mockito.when(bmService.deletePost(Mockito.any())).thenReturn(Optional.ofNullable(false));
		mockMvc.perform(MockMvcRequestBuilders.delete("/auth/user/deletePost/1").accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(MockMvcResultMatchers.status().isBadRequest())
				.andReturn().getResponse().getContentAsString();
	}

	@Test
	@WithMockUser
	public void deletePostComment() throws UnsupportedEncodingException, Exception {
		Mockito.when(bmService.deletePostComment(Mockito.any())).thenReturn(Optional.ofNullable(true));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/auth/user/deletePostComment/1")
						.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		ApiResponse readValue = objectMapper.readValue(contentAsString, ApiResponse.class);
		assertEquals("Delete PostComment Successfull", readValue.getMessage());

	}

	@Test
	@WithMockUser
	public void deletePostComment_Returns400() throws UnsupportedEncodingException, Exception {
		Mockito.when(bmService.deletePostComment(Mockito.any())).thenReturn(Optional.ofNullable(false));
		mockMvc.perform(MockMvcRequestBuilders.put("/auth/user/deletePostComment/1")
				.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();
	}

	

	@Test
	@WithMockUser
	void testUpdateUser() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		UserUpdateDto updateDto = UserUpdateDto.builder().firstName("vinay").middleName("kumar").lastName("Thoutam")
				.email("a@a").intro("--").mobile("1234568").profile("profile").passwordHash("qwerty").build();
		Mockito.when(bmService.updateUser(Mockito.any(), Mockito.any())).thenReturn(Optional.ofNullable(true));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/auth/user/updateUser/1").accept(MediaType.APPLICATION_JSON_VALUE)
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(updateDto)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		ApiResponse<String> readValue = objectMapper.readValue(contentAsString, ApiResponse.class);
		assertEquals("Update User Successfull", readValue.getMessage());

	}

	@Test
	@WithMockUser
	void testUpdateUser_Returns400() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		UserUpdateDto updateDto = UserUpdateDto.builder().firstName("vinay").middleName("kumar").lastName("Thoutam")
				.email("a@a").intro("--").mobile("1234568").profile("profile").passwordHash("qwerty").build();
		Mockito.when(bmService.updateUser(Mockito.any(), Mockito.any())).thenReturn(Optional.ofNullable(null));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/auth/user/updateUser/1").accept(MediaType.APPLICATION_JSON_VALUE)
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(updateDto)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();

	}


	@Test
	@WithMockUser
	void testPostRegistration() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		List<PostDto> postDtos = new ArrayList<PostDto>();
		PostDto postDto = new PostDto();
		postDtos.add(postDto);

		PostDto postDto1 = PostDto.builder().authorId(1).categoryDto(null).content("content")
				.createdAt(LocalDateTime.now()).data(null).fileName("resume").fileType("pdf").metaTitle("metatittle")
				.parentId(1).userDto(null).updatedAt(null).title("tittle").tagDto(null).summary("summary").slug("slug")
				.build();

		Mockito.when(bmService.postRegistration(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(postDtos));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.post("/auth/user/postRegistration/1")
						.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(postDto)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		ApiResponse<String> readValue = objectMapper.readValue(contentAsString, ApiResponse.class);
		assertEquals("Post Registration Successfull", readValue.getMessage());

	}

	@Test
	@WithMockUser
	void testPostRegistration_Return400() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		PostDto postDto = PostDto.builder().authorId(1).categoryDto(null).content("content")
				.createdAt(LocalDateTime.now()).data(null).fileName("resume").fileType("pdf").metaTitle("metatittle")
				.parentId(1).userDto(null).updatedAt(null).title("tittle").tagDto(null).summary("summary").slug("slug")
				.build();

		Mockito.when(bmService.postRegistration(Mockito.any(), Mockito.any())).thenReturn(Optional.ofNullable(null));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.post("/auth/user/postRegistration/1")
						.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(postDto)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();

	}

	@Test
	@WithMockUser
	void testPostCommentRegister() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		PostCommentDto postCommentDto = PostCommentDto.builder().content("content").createdAt(LocalDateTime.now())
				.parentId("P01").postDto(null).postId("P001").published("Public").publishedAt(LocalDateTime.now())
				.title("Tittle").build();

		Mockito.when(bmService.commentRegistration(Mockito.any(), Mockito.any())).thenReturn(Optional.ofNullable(true));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.post("/auth/user/postComment/1").accept(MediaType.APPLICATION_JSON_VALUE)
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(postCommentDto)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		ApiResponse<String> readValue = objectMapper.readValue(contentAsString, ApiResponse.class);
		assertEquals("PostComment added Successfull", readValue.getMessage());

	}

	@Test
	@WithMockUser
	void testPostCommentRegister_Return400() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		PostCommentDto postCommentDto = PostCommentDto.builder().content("content").createdAt(LocalDateTime.now())
				.parentId("P01").postDto(null).postId("P001").published("Public").publishedAt(LocalDateTime.now())
				.title("Tittle").build();

		Mockito.when(bmService.commentRegistration(Mockito.any(), Mockito.any()))
				.thenReturn(Optional.ofNullable(false));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.post("/auth/user/postComment/1").accept(MediaType.APPLICATION_JSON_VALUE)
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(postCommentDto)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();

	}

	@Test
	@WithMockUser
	void testupdatePostComment() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		UpdatePostCommentDto updatePostCommentDto = UpdatePostCommentDto.builder().content("content").parentId("P01")
				.postDto(null).published("Private").publishedAt(LocalDateTime.now()).title("TITTLE").build();
		Mockito.when(bmService.updatePostComment(Mockito.any(), Mockito.any())).thenReturn(Optional.ofNullable(true));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/auth/user/updatePostComment/1")
						.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(updatePostCommentDto)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		ApiResponse<String> readValue = objectMapper.readValue(contentAsString, ApiResponse.class);
		assertEquals("Update Post Successfull", readValue.getMessage());

	}

	@Test
	@WithMockUser
	void testupdatePostComment_Return400() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		UpdatePostCommentDto updatePostCommentDto = UpdatePostCommentDto.builder().content("content").parentId("P01")
				.postDto(null).published("Private").publishedAt(LocalDateTime.now()).title("TITTLE").build();
		Mockito.when(bmService.updatePostComment(Mockito.any(), Mockito.any())).thenReturn(Optional.ofNullable(false));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.put("/auth/user/updatePostComment/1")
						.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(updatePostCommentDto)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();

	}

	@Test
	@WithMockUser
	void testreadPostsAndComments() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		PostDto postDto = PostDto.builder().authorId(1).categoryDto(null).content("content")
				.createdAt(LocalDateTime.now()).data(null).fileName("resume").fileType("pdf").metaTitle("metatittle")
				.parentId(1).userDto(null).updatedAt(null).title("tittle").tagDto(null).summary("summary").slug("slug")
				.build();
		Mockito.when(bmService.readPostAndComments(Mockito.any())).thenReturn(Optional.ofNullable(postDto));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.get("/auth/user/readPostAndComments/1")
						.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(postDto)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		ApiResponse<String> readValue = objectMapper.readValue(contentAsString, ApiResponse.class);
		assertEquals("PostsAndComments are Available", readValue.getMessage());

	}

	@Test
	@WithMockUser
	void testreadPostsAndComments_Returns400() throws JsonProcessingException, UnsupportedEncodingException, Exception {
		PostDto postDto = PostDto.builder().authorId(1).categoryDto(null).content("content")
				.createdAt(LocalDateTime.now()).data(null).fileName("resume").fileType("pdf").metaTitle("metatittle")
				.parentId(1).userDto(null).updatedAt(null).title("tittle").tagDto(null).summary("summary").slug("slug")
				.build();
		Mockito.when(bmService.readPostAndComments(Mockito.any())).thenReturn(Optional.ofNullable(null));
		String contentAsString = mockMvc
				.perform(MockMvcRequestBuilders.get("/auth/user/readPostAndComments/1")
						.accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE)
						.content(objectMapper.writeValueAsString(postDto)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn().getResponse()
				.getContentAsString();

	}

}