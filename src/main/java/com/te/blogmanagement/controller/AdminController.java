package com.te.blogmanagement.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.te.blogmanagement.customexception.DeleteUserUnsuccessfullException;
import com.te.blogmanagement.customexception.UserDataListUnAvailableException;
import com.te.blogmanagement.dto.UserDto;
import com.te.blogmanagement.response.ApiResponse;
import com.te.blogmanagement.service.BmService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/auth")
public class AdminController {
	private final BmService bmService;

	@GetMapping(path = "/userDataList")
	public ResponseEntity<ApiResponse<Object>> userDataList() {
		Optional<List<UserDto>> userDataList = bmService.UserDataList();
		if (userDataList.isPresent()) {
			return ResponseEntity.ok()
					.body(ApiResponse.builder().message("User DataList is Available").data(userDataList.get()).build());
		}
		throw new UserDataListUnAvailableException("User DataList is Not Available");
	}

	@PutMapping(path = "/deleteUser/{id}")
	public ResponseEntity<ApiResponse<Object>> deleteUser(@PathVariable(name = "id") Integer id) {
		Optional<Boolean> deleteUser = bmService.deleteUser(id);
		if (deleteUser.get().equals(true)) {
			return ResponseEntity.ok().body(ApiResponse.builder().message("Delete User Successfull").build());
		}
		throw new DeleteUserUnsuccessfullException("Delete User UnSuccessfull");

	}

}
