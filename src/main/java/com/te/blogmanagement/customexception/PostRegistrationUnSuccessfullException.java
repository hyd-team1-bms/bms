package com.te.blogmanagement.customexception;

public class PostRegistrationUnSuccessfullException extends RuntimeException {
	public PostRegistrationUnSuccessfullException(String message) {
		super(message);
	}

}
